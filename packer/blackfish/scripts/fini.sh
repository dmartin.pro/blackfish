#!/bin/bash -ex
CLEANUP=${CLEANUP:-0}
WORKDIR=${WORKDIR:-/home/core}
CONSUL_VERSION=0.6.4

#DOWNLOADS
wget -O "$WORKDIR"/docker2aci.tar.gz https://github.com/appc/docker2aci/releases/download/v0.9.3/docker2aci-v0.9.3.tar.gz
wget -O "$WORKDIR"/consul.zip https://releases.hashicorp.com/consul/${CONSUL_VERSION}/consul_${CONSUL_VERSION}_linux_amd64.zip
wget -O "$WORKDIR"/config.yml https://raw.githubusercontent.com/docker/distribution/master/cmd/registry/config-example.yml
wget -O "$WORKDIR"/telegraf.tar.gz https://dl.influxdata.com/telegraf/releases/telegraf-0.13.1_linux_amd64.tar.gz

#PREPARE IMAGES
pushd "$WORKDIR"
tar -xzf ./docker2aci.tar.gz
sudo mkdir -p /opt/blackfish/acis
sudo cp image.list /opt/blackfish/acis
for i in ./*.docker; do
    if [ -f "$i" ]; then
        mkdir ./tmp
        TMPDIR=./tmp ./docker2aci-v0.9.3/docker2aci "$i"
        sudo mv "${i%*.docker}.aci" /opt/blackfish/acis
        rm -Rf "$i" ./tmp
    fi
done
popd

#CREATE DIRS
sudo mkdir -p /opt/blackfish
sudo mkdir -p /etc/blackfish/docker.conf.d
sudo mkdir -p /etc/systemd/system/docker.service.d
sudo mkdir -p /etc/docker/registry

#INSTALL FILES
pushd /opt/blackfish
sudo unzip -o "$WORKDIR"/consul.zip
sudo rm "$WORKDIR"/consul.zip
popd
sudo cp "$WORKDIR"/image.list /etc/blackfish

TMPDIR=$(mktemp -d)
pushd $TMPDIR
tar -xzf "$WORKDIR"/telegraf.tar.gz
sudo mv ./telegraf-0.13.1-1/usr/bin/telegraf /opt/blackfish
sudo chmod +x /opt/blackfish/telegraf
popd

sudo mv "$WORKDIR"/{consul,registry,telegraf,haproxy,registrator,swarm-manager,swarm-agent,flocker-control,flocker-container-agent,flocker-dataset-agent,flocker-docker-plugin}-manage /opt/blackfish/
sudo mv "$WORKDIR"/functions.sh /opt/blackfish/
sudo mv "$WORKDIR"/consul-cli /opt/blackfish/
sudo mv "$WORKDIR"/docker-configurator /opt/blackfish/
sudo mv "$WORKDIR"/blackfish-init /opt/blackfish/
sudo mv "$WORKDIR"/journald-forwarder /opt/blackfish/
sudo mv "$WORKDIR"/telegraf.conf /opt/blackfish/telegraf.conf

sudo chmod +x /opt/blackfish/*-manage
sudo chmod +x /opt/blackfish/docker-configurator
sudo chmod +x /opt/blackfish/blackfish-init
sudo chmod +x /opt/blackfish/consul-cli
sudo chmod +x /opt/blackfish/journald-forwarder

sudo mv "$WORKDIR"/{blackfish,telegraf,haproxy,journald-forwarder,consul,registrator,docker-configurator,swarm-manager,swarm-agent,registry,flocker-control,flocker-dataset-agent,flocker-container-agent,flocker-docker-plugin}.service /etc/systemd/system/
sudo mv "$WORKDIR"/blackfish.path /etc/systemd/system/

sudo mv "$WORKDIR"/config.yml /etc/docker/registry
sudo mv "$WORKDIR"/60-swarm.conf /etc/blackfish/docker.conf.d/


# SETUP SYSCTL
sudo tee -a /etc/sysctl.d/default.conf > /dev/null <<EOF
# The first set of settings is intended to open up the network stack performance by
# raising memory limits and adjusting features for high-bandwidth/low-latency
# networks.
net.ipv4.tcp_rmem = 4096 87380 16777216
net.ipv4.tcp_wmem = 4096 65536 16777216
net.core.rmem_max = 16777216
net.core.wmem_max = 16777216
net.core.netdev_max_backlog = 2500
net.core.somaxconn = 65000
net.ipv4.tcp_ecn = 0
net.ipv4.tcp_window_scaling = 1
net.ipv4.ip_local_port_range = 10000 65535

# this block is designed for and only known to work in a single physical DC
# TODO: validate on multi-DC and provide alternatives
net.ipv4.tcp_syncookies = 0
net.ipv4.tcp_timestamps = 0
net.ipv4.tcp_sack = 0
net.ipv4.tcp_fack = 1
net.ipv4.tcp_dsack = 1
net.ipv4.tcp_orphan_retries = 1

# significantly reduce the amount of data the kernel is allowed to store
# in memory between fsyncs
# dirty_background_bytes says how many bytes can be dirty before the kernel
# starts flushing in the background. Set this as low as you can get away with.
# It is basically equivalent to trickle_fsync=true but more efficient since the
# kernel is doing it. Older kernels will need to use vm.dirty_background_ratio
# instead.
vm.dirty_background_bytes = 10485760

# Same deal as dirty_background but the whole system will pause when this threshold
# is hit, so be generous and set this to a much higher value, e.g. 1GB.
# Older kernels will need to use dirty_ratio instead.
vm.dirty_bytes = 1073741824

# disable zone reclaim for IO-heavy applications like Cassandra
vm.zone_reclaim_mode = 0

# there is no good reason to limit these on server systems, so set them
# to 2^31 to avoid any issues
# Very large values in max_map_count may cause instability in some kernels.
fs.file-max = 1073741824
vm.max_map_count = 1073741824

# only swap if absolutely necessary
# some kernels have trouble with 0 as a value, so stick with 1
vm.swappiness = 1
EOF

# SETUP ULIMITS
tee -a /tmp/limits.conf > /dev/null <<EOF
* - nofile     1000000
* - memlock    unlimited
* - fsize      unlimited
* - data       unlimited
* - rss        unlimited
* - stack      unlimited
* - cpu        unlimited
* - nproc      unlimited
* - as         unlimited
* - locks      unlimited
* - sigpending unlimited
* - msgqueue   unlimited
EOF
sudo cp -f /tmp/limits.conf /etc/security/

# SETUP SYSTEMD
sudo systemctl enable blackfish.path
sudo systemctl stop update-engine.service
sudo systemctl stop locksmithd.service
sudo systemctl disable update-engine.service
sudo systemctl disable locksmithd.service
sudo systemctl mask update-engine.service
sudo systemctl mask locksmithd.service
sudo systemctl disable docker.service

# SETUP debug tools
toolbox dnf install -y htop dstat fio

#cleanup
if [[ "$CLEANUP" =~ [Y|y|1] ]]; then
    rm -Rf ~/.ssh/authorized_keys
    sudo rm -Rf /etc/docker/key.json
    sudo rm -Rf /var/lib/consul
fi
