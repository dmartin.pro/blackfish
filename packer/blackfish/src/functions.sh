#!/bin/bash
if [[ -f "/etc/blackfish/blackfish.conf" ]]; then
    source /etc/blackfish/blackfish.conf
else
    log user.error "couldn't find configuration file"
    exit 1
fi

BLACKFISH_HOME=${BLACKFISH_HOME:-/opt/blackfish}

SCRIPT_FILE="$0"
if [[ "$SCRIPT_FILE" == "sh" ]] || [[ "$SCRIPT_FILE" == "-bash" ]] || [[ "$SCRIPT_FILE" == "-zsh" ]]; then
    echo "sourcing from sh|bash|zsh" >&2
    echo "SERVICE_NAME will be set to shell_$$".
    SERVICE_NAME="shell_$$"
    UUID_FILE=/var/run/"$SERVICE_NAME".uuid
else
    FILENAME="$(basename "$0")"
    SERVICE_NAME="${FILENAME%*-manage}"
    UUID_FILE=/var/run/"$SERVICE_NAME".uuid
fi

ADMIN_NETWORK=${ADMIN_NETWORK:-default}
PUBLIC_NETWORK=${PUBLIC_NETWORK:-default}

STACK_NAME=${STACK_NAME:-blackfish}
DATACENTER=${DATACENTER:-dc1}

log(){
   logger -s -t "$SERVICE_NAME" -p "$@"
}

getipaddrfornetwork(){
    NETWORK="$1"
    # Keep trying to retrieve IP addr until it succeeds. Timeouts after 1m
    now=$(date +%s)
    timeout=$(( now + 60 ))
    set +e
    while :; do
        if [[ $timeout -lt $(date +%s) ]]; then
            log user.error "Could not retrieve IP Address. Exiting"
            exit 5
        fi
        ip route | grep -q "^$NETWORK"
        [ $? -eq 0 ] && break
        sleep 1
    done

    ip route | grep "^$NETWORK" | sed 's/.*src \([0-9\.]*\) .*/\1/g'
}

getpubipaddr(){
    getipaddrfornetwork "$PUBLIC_NETWORK"
}

gethostadminipaddr(){
    getipaddrfornetwork "$ADMIN_NETWORK"
}

stop_rkt() {
    if [ -f "$UUID_FILE" ]; then
        sudo machinectl kill "rkt-$(cat "$UUID_FILE")"
    fi
}

rm_rkt() {
    if [ -f "$UUID_FILE" ]; then
        rkt rm --uuid-file "$UUID_FILE"
    fi
}

run_curl(){
    METHOD=$1
    shift
    /usr/bin/curl --fail \
         --cacert /etc/blackfish/certs/ca.pem \
         --cert /etc/blackfish/certs/client.pem \
         --key /etc/blackfish/certs/client-key.pem \
         -X"$METHOD" "$@"
}

consul() {
    METHOD=$1
    shift
    path=$1
    shift
    run_curl "$METHOD" "https://$(hostname).node.$DATACENTER.$STACK_NAME:8500/v1$path" "$@"
}

consul-unlock(){
    SESSION_ID=$(cat /var/run/"$SERVICE_NAME".session)
    if [ ! -z "$SESSION_ID" ]; then
        consul PUT "/kv/blackfish/$SERVICE_NAME-lock?release=$SESSION_ID" 2>/dev/null
    fi
}

consul-new-session(){
    TMPFILE=$(mktemp)
    cat > "$TMPFILE" <<EOF
{"Name":"$(hostname)", "TTL": "120s", "LockDelay" : "120s"}
EOF
    consul PUT "/session/create" \
           -d @"$TMPFILE" 2>/dev/null | jq '.ID' | sed 's/"//g' > /var/run/"$SERVICE_NAME".session
    rm "$TMPFILE"
    cat /var/run/"$SERVICE_NAME".session
}

consul-lock(){
    touch /var/run/"$SERVICE_NAME".session
    SESSION_ID=$(cat /var/run/"$SERVICE_NAME".session)
    if [ ! -z "$SESSION_ID" ]; then
        consul PUT "/session/renew/$SESSION_ID"
        if [ $? != 0 ]; then
            SESSION_ID=$(consul-new-session)
        fi
    else
        SESSION_ID=$(consul-new-session)
    fi

    LOCKED=$(consul PUT "/kv/blackfish/$SERVICE_NAME-lock?acquire=$SESSION_ID" 2>/dev/null)

    if [[ "$LOCKED" != "true" ]]; then
        log user.error "lock already acquired".
        exit 1
    else
        log user.info "lock acquired for session $SESSION_ID"
    fi
}

aci_name(){
    if [ ! -f "$BLACKFISH_HOME"/acis/image.list ]; then
        echo "image.list not found in $BLACKFISH_HOME/acis">&2
        exit 1
    fi

    IMAGE_NAME=$(grep "^$1:.*" "$BLACKFISH_HOME"/acis/image.list)
    if [ -z "$IMAGE_NAME" ]; then
        echo "image not found in $BLACKFISH_HOME/acis/image.list">&2
        exit 1
    fi

    ACI_NAME=$(rkt image list --fields=name | grep "$IMAGE_NAME")
    if [ -z "$ACI_NAME" ]; then
        echo "aci $ACI_NAME not found in rkt image list">&2
        exit 1
    fi
    echo "$ACI_NAME"

}

aci_id(){
    if [ ! -f "$BLACKFISH_HOME"/acis/image.list ]; then
        echo "image.list not found in $BLACKFISH_HOME/acis">&2
        exit 1
    fi

    IMAGE_NAME=$(grep "^$1:.*" "$BLACKFISH_HOME"/acis/image.list)
    if [ -z "$IMAGE_NAME" ]; then
        echo "image not found in $BLACKFISH_HOME/acis/image.list">&2
        exit 1
    fi

    ACI_ID=$(rkt image list --fields=id,name | awk '$0~v {print $1}' v="$IMAGE_NAME")
    if [ -z "$ACI_ID" ]; then
        echo "aci $ACI_ID not found in rkt image list">&2
        exit 1
    fi
    echo "$ACI_ID"
}

cidr2mask() {
    local i mask=""
    local full_octets=$(($1/8))
    local partial_octet=$(($1%8))

    for ((i=0;i<4;i+=1)); do
        if [ $i -lt $full_octets ]; then
            mask+=255
        elif [ $i -eq $full_octets ]; then
            mask+=$((256 - 2**(8-$partial_octet)))
        else
            mask+=0
        fi
        test $i -lt 3 && mask+=.
    done

    echo $mask
}

mask2cidr() {
    nbits=0
    IFS=.
    for dec in $1 ; do
        case $dec in
            255) let nbits+=8;;
            254) let nbits+=7;;
            252) let nbits+=6;;
            248) let nbits+=5;;
            240) let nbits+=4;;
            224) let nbits+=3;;
            192) let nbits+=2;;
            128) let nbits+=1;;
            0);;
            *) echo "Error: $dec is not recognised"; exit 1
        esac
    done
    echo "$nbits"
}
