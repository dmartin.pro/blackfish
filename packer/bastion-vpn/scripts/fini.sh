#!/bin/bash

docker pull busybox:latest
docker pull kylemanna/openvpn:latest

sudo mkdir -p /opt/bin

sudo mv /tmp/init-ovpndata.sh /opt/bin
sudo mv /tmp/ovpn-client-config.sh /opt/bin
sudo mv /tmp/nat-forwarding /opt/bin 

sudo chmod +x /opt/bin/*

sudo mv /tmp/openvpn-data.service /etc/systemd/system
sudo mv /tmp/openvpn.service /etc/systemd/system
sudo mv /tmp/nat-forwarding.service /etc/systemd/system
sudo systemctl enable openvpn-data.service
sudo systemctl enable openvpn.service
sudo systemctl enable nat-forwarding.service

rm -Rf ~/.ssh/authorized_keys

sudo systemctl stop update-engine.service
sudo systemctl disable update-engine.service
sudo systemctl stop locksmithd.service
sudo systemctl disable locksmithd.service
